# Cube Commander

Cube Commander is a mobile game created by a bunch of friends and myself for our final your university project. 
You can find the game [on the Google Play store](https://play.google.com/store/apps/details?id=com.PlaceHolderProductions.CubeCommander2&hl=en&gl=US) and download it if you have an android device. 

There are lots of things we could have done to improve the game, especially now looking back with the knowledge and experience I have gained, and also hindsight exists.

My main duties we're game design, sound design, project management and some development. The game was developed in Unity Game Engine. 
